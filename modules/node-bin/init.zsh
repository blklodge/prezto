# Add current node_modules/.bin to PATH

function npm_bin_to_path_chpwd() {
    # if there is an __ORIG_PATH, then reset before modification
    if [ -n "$__ORIG_PATH" ]; then
        NEW_PATH=$__ORIG_PATH
    else
        __ORIG_PATH=$PATH
    fi

    NEW_PATH=$(npm bin):$__ORIG_PATH
    export PATH=$NEW_PATH
}

chpwd_functions=(${chpwd_functions[@]} "npm_bin_to_path_chpwd")
