# Activate any virtualenv specified in .venvinfo

#function activate_venv_cwd() {
function activate_venv_chpwd() {
    # Activate virtualenvs on change dir if we are not in one
    if [ -f .venv ]; then
        # Activate venv specified in .venv if it's a file
        venvdir=$(cat .venv)
    elif [ -f .venvinfo ]; then
        # Activate venv specified in .venvinfo if it's a file
        venvdir=$(cat .venvinfo)
    elif [ -d .venv ]; then
        venvdir=".venv"
    fi
    if [ ! $VIRTUAL_ENV ] && [[ -e "$venvdir/bin/activate" ]]; then
        source $venvdir/bin/activate
        printf "Activating virtualenv in $venvdir\n"
    fi
}

#autoload add-zsh-hook
#add-zsh-hook chpwd activate_venv_cwd

## Tell thie term about initial directory
#activate_venv_cwd
chpwd_functions=(${chpwd_functions[@]} "activate_venv_chpwd")
